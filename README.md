# pruebaTecnica

## INSTRUCCIONES

Se adjunta .json el cual se debe importar en postman para hacer pruebas a la API.

 - Iniciar base de datos mongo.
 - Abrir carpeta con codigo en editor de codigo de preferencia.
 - Ejecutar en la lina de comandos "npm start" para iniciar la API.
 - Importar el archivo PRUEBA.postman.collection.json a POSTMAN para realizar pruebas.

## CONSTRUIDO CON:

 - Mongo
 - Node.js
 - Dotenv
 - Express
