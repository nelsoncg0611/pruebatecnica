//Libreria para añadir variables de entorno.
require('dotenv').config();

//Config conexion el server.
const express = require('express');
const app = express();
const PORT = process.env.PORT;
app.use(express.json());

//Conexion a la base de datos.
require("./database");

//Inicio de server
app.listen(PORT, () => { console.log("index iniciado en el puerto: " + PORT); });

//De
const rutasUsuarios = require("./route/usuarios.route");

app.use('/usuarios', rutasUsuarios);
