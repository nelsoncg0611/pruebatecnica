const mongoose = require('mongoose');

//Llamado y conexion a la base de datos.
const { MONGODB_HOST_AND_PORT, MONGODB_NAME_DATABASE } = process.env;
const URI = `mongodb://${MONGODB_HOST_AND_PORT}/${MONGODB_NAME_DATABASE}`;

const db = mongoose.connection;

(async () => {
    try {
        await mongoose.connect(URI, { useUnifiedTopology: true, useNewUrlParser: true });
}   catch (err) {
        console.log("ERROR AL CORRER BASE DE DATOS >>>>>>>" + err);
}
})
();

    db.on('open', async () => {console.log("conectado a la bd")});