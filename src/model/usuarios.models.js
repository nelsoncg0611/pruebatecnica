//Modelo para el formulario de datos del usuario.

const mongoose = require("mongoose");

const usuarioSchema = new mongoose.Schema ({
    "nombre": {
        type: String,
        required: true,
    },
    "apellido": {
        type: String,
        required: true,
    },
    "tipo_documento": {
        type: String,
        required: true,
    },
    "numero_documento": {
        type: Number,
        required: true,
    },
    "email": {
        type: String,
        required: true,
    },
    "celular": {
        type: Number,
        required: true,
    },
    "fecha_nacimiento": {
        type: Date,
        required: true,
    },
    "url_archivo": {
        type: String,
        require: true,
    }
});

usuarioSchema.method.url_img = function url_img (filename) {
    this.url_img = `http://localhost:3000/archivos/${filename}`;
}

module.exports = mongoose.model("Usuarios", usuarioSchema);
