const express = require('express');
const route = express.Router();
const usuarioModelo = require('../model/usuarios.models');

//Ruta para obtener todos los usuarios.
route.get('/lista', async (req, res) => {
    res.json(await usuarioModelo.find());
});

//Ruta para agregar un nuevo usuario.
route.post('/usernew', async (req, res) => {
    try {
        const { nombre, apellido, tipo_documento, numero_documento, email, celular, fecha_nacimiento, url_archivo } = req.body;
        if (nombre && apellido && tipo_documento && numero_documento && email && celular && fecha_nacimiento && url_archivo) {
            const userNew = await new usuarioModelo({
                nombre,
                apellido,
                tipo_documento,
                numero_documento,
                email,
                celular,
                fecha_nacimiento,
                url_archivo
            });
            await userNew.save();
            res.json({
                mensaje: "usuario creado exitosamente",
                usuario: { userNew },
            })
        } else { res.json('no se han enviado todos los datos requeridos') };
    } catch (err) {
        res.status(500).json('INTERNAL SERVER ERROR');
    }
});

//Ruta para modificar email del usuario por medio de su Id.
route.put('/change_email/:id', async (req, res) => {
    try {
        const { id: _id } = req.params;
        const { email } = req.body;
        if (email) {
            const userUp = await usuarioModelo.findByIdAndUpdate({ _id }, { email });
            res.json('Se actualizo correctamente el email');
        } else { res.json('Se debe ingresar un email') }
    } catch (err) {
        if (err.name = 'CastError'){
            res.status(400).json('Id de usuario invalido');
        } else {
        res.status(500).json('INTERNAL SERVER ERROR');
};
}
})

//Ruta para eliminar un usuario por medio de su Id.
route.delete('/delete_user/:id', async (req, res) => {
    try {
    const { id: _id } = req.params;
    const userDel = await usuarioModelo.findByIdAndDelete({_id});
    console.log(userDel)
    if (userDel == null){
        res.status(400).json('Id de usuario invalido');
    } else {
        res.status(201).json('Usuario eliminado correctamente')
    }
} catch (err) {
    if (err.name = 'CastError'){
        res.status(400).json('Id de usuario invalido');
    } else {
    res.status(500).json('INTERNAL SERVER ERROR');
}
};
})

module.exports = route;